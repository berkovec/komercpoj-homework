package com.komercpoj.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

/**
 * Cost service test.
 * Created by Michal Berkovec on 1.12.2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CostServiceTest {

    @Autowired
    private ICostsService costsService;

    /**
     * Tests functionality of calculation of person costs.
     */
    @Test
    public void getCostsTest() {
        LocalDate birthDate = LocalDate.of(1982, 11, 24);
        float costs = costsService.getCosts(birthDate, 1200000, 0.5f);
        assert costs == 1.56E7f;
    }
}
