package com.komercpoj.service;

import com.komercpoj.model.SalaryEntity;

import java.util.List;

/**
 * Service interface for managing annual salaries.
 * Created by Michal Berkovec on 30.11.2016.
 */
public interface ISalaryService {

    /**
     * Stores salary entity.
     * @param salaryEntity salary entity
     * @return stored salary entity
     */
    SalaryEntity storeSalary(SalaryEntity salaryEntity);

    /**
     * Finds all stored salaries
     * @return all stored salaries
     */
    List<SalaryEntity> getAllSalaries();
}
