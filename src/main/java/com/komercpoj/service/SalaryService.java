package com.komercpoj.service;

import com.komercpoj.dao.ISalaryDao;
import com.komercpoj.model.SalaryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Service implementation for managing annual salaries.
 * Created by Michal Berkovec on 30.11.2016.
 */
@Component
public class SalaryService implements ISalaryService {

    /**
     * DAO.
     */
    private ISalaryDao salaryDao;

    @Autowired
    public SalaryService(ISalaryDao salaryDao) {
        this.salaryDao = salaryDao;
    }

    @Transactional
    public SalaryEntity storeSalary(SalaryEntity salaryEntity) {
        salaryDao.create(salaryEntity);
        return salaryEntity;
    }

    public List<SalaryEntity> getAllSalaries() {
        return salaryDao.findAll();
    }
}
