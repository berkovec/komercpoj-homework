package com.komercpoj.service;

import com.komercpoj.dao.ISalaryDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Service implementation for operation with costs.
 * Created by Michal Berkovec on 30.11.2016.
 */
@Component
public class CostService implements ICostsService {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CostService.class);

    /**
     * DAO.
     */
    ISalaryDao salaryDao;

    @Autowired
    public CostService(ISalaryDao salaryDao) {
        this.salaryDao = salaryDao;
    }

    @Override
    public float getCosts(LocalDate birthDate, float salary, float currency) {
        long years = birthDate.until(LocalDate.now(), ChronoUnit.YEARS);

        float inputAge = 60f - years;

        if (inputAge < 0) {
            inputAge = 0f;
        }

        float costs = inputAge * salary * currency;
        LOGGER.info("Costs for date of birth {}, annual salary {} and currency conversion rate {} is {}.", birthDate, salary, currency, costs);
        return costs;
    }

}
