package com.komercpoj.service;

import java.time.LocalDate;

/**
 * Service interface for operation with costs.
 * Created by Michal Berkovec on 30.11.2016.
 */
public interface ICostsService {

    /**
     * Count costs.
     *
     * @param birthDate date of birth
     * @param salary annual salary
     * @param currency currency converion rate for 1 CZK
     * @return costs for person
     */
    float getCosts(LocalDate birthDate, float salary, float currency);

}
