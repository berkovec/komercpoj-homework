package com.komercpoj.service.hl;

import com.komercpoj.model.SalaryEntity;
import com.komercpoj.model.vo.Form;
import com.komercpoj.service.ISalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * High level service implementation for managing salaries.
 * Created by Michal Berkovec on 30.11.2016.
 */
@Component
public class SalaryHLService implements ISalaryHLService {

    /**
     * Salary service.
     */
    private ISalaryService salaryService;

    @Autowired
    public SalaryHLService(ISalaryService salaryService) {
        this.salaryService = salaryService;
    }

    @Transactional
    public void storeSalary(Form form) {
        SalaryEntity salaryEntity = new SalaryEntity();
        salaryEntity.setDate(new Date());
        salaryEntity.setAnnualSalary(form.getSalary());

        salaryService.storeSalary(salaryEntity);
    }
}
