package com.komercpoj.service.hl;

import com.komercpoj.model.vo.Form;

/**
 * High level service interface for managing salaries.
 * Created by Michal Berkovec on 30.11.2016.
 */
public interface ISalaryHLService {

    /**
     * Stores salary entity cretaed from given form value object.
     * @param form form value object
     */
    void storeSalary(Form form);
}
