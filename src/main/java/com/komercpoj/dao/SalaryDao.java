package com.komercpoj.dao;

import com.komercpoj.model.SalaryEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * DAO implementation for Salary entity.
 * Created by Michal Berkovec on 30.11.2016.
 */
@Component
public class SalaryDao implements ISalaryDao {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SalaryDao.class);


    @PersistenceContext
    private EntityManager entityManager;

    public void create(SalaryEntity salaryEntity) {
        entityManager.persist(salaryEntity);
        LOGGER.info("Salary entity {} persisted.", salaryEntity);
    }

    public List<SalaryEntity> findAll() {
        Query query = entityManager.createQuery("SELECT e FROM SalaryEntity e");
        return (List<SalaryEntity>) query.getResultList();
    }
}
