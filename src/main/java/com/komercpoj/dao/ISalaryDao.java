package com.komercpoj.dao;

import com.komercpoj.model.SalaryEntity;

import java.util.List;

/**
 * DAO interface for Salary entity.
 * Created by Michal Berkovec on 30.11.2016.
 */
public interface ISalaryDao {

    /**
     * Creates record in DB for salary entity.
     * @param salaryEntity salary entity
     */
    void create(SalaryEntity salaryEntity);

    /**
     * Finds all salary entities.
     * @return all salary entities
     */
    List<SalaryEntity> findAll();
}
