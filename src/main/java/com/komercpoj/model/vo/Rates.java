package com.komercpoj.model.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Currency rates POJO.
 * Created by Michal Berkovec on 30.11.2016.
 */
public class Rates {

    /**
     * Currency conversion rate for base currency rate to USD.
     */
    @JsonProperty(value = "USD")
    private float rateUSD;

    /**
     * Currency conversion rate for base currency rate to EUR.
     */
    @JsonProperty(value = "EUR")
    private float rateEUR;

    // Getters and setters
    public float getRateUSD() {
        return rateUSD;
    }

    public void setRateUSD(float rateUSD) {
        this.rateUSD = rateUSD;
    }

    public float getRateEUR() {
        return rateEUR;
    }

    public void setRateEUR(float rateEUR) {
        this.rateEUR = rateEUR;
    }
}
