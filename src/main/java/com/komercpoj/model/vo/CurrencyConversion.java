package com.komercpoj.model.vo;

/**
 * Currency conversion POJO.
 * Created by Michal Berkovec on 30.11.2016.
 */
public class CurrencyConversion {

    /**
     * Base currency.
     */
    private String base;

    /**
     * Currency conversion rates.
     */
    private Rates rates;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }
}
