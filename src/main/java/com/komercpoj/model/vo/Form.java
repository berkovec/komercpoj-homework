package com.komercpoj.model.vo;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Form value object.
 * Created by Michal Berkovec on 30.11.2016.
 */
public class Form {

    /**
     * Date of birth.
     */
    private Date birthDate;

    /**
     * Annual salary.
     */
    private float salary;

    /**
     * Costs for person in CZK.
     */
    private float costsCZK;

    /**
     * Costs for person in EUR.
     */
    private float costsEUR;

    /**
     * Costs for person in USD.
     */
    private float costsUSD;

    // Getters and setters
    public Date getBirthDate() {
        return birthDate;
    }

    public LocalDate getBirtLocalDate() {
        if (birthDate != null) {
            return birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } else {
            return null;
        }
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public float getSalary() {
        return salary;
    }

    public float getCostsCZK() {
        return costsCZK;
    }

    public void setCostsCZK(float costsCZK) {
        this.costsCZK = costsCZK;
    }

    public float getCostsEUR() {
        return costsEUR;
    }

    public void setCostsEUR(float costsEUR) {
        this.costsEUR = costsEUR;
    }

    public float getCostsUSD() {
        return costsUSD;
    }

    public void setCostsUSD(float costsUSD) {
        this.costsUSD = costsUSD;
    }
}
