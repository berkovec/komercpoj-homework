package com.komercpoj.model;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Date;

/**
 * Salary entity.
 * Created by Michal Berkovec on 30.11.2016.
 */
@Entity
@Table(name="Salary")
public class SalaryEntity {

    /**
     * Primary key.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Date of calculation.
     */
    @Column(name = "datum_vypoctu")
    private Date date;

    /**
     * Annual salary.
     */
    @Column(name = "rocni_mzda")
    private float annualSalary;

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(float annualSalary) {
        this.annualSalary = annualSalary;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("date", date)
                .append("annualSalary", annualSalary)
                .toString();
    }
}
