package com.komercpoj.spring;

import com.komercpoj.rest.ICurrencyAPI;
import feign.Feign;
import feign.Logger;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Application context configuration.
 * Created by Michal Berkovec Berkovec on 20.10.2016.
 */
@Configuration
public class AppConfiguration {
    /* --- Properties --- */
    @Value("${currency.api.url}")
    private String currencyAPIUrl;

    @Value("${feign.logLevel}")
    private Logger.Level feignLogLevel;

    @Bean
    ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }

    @Bean
    public ICurrencyAPI currencyAPI() {
        return Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .logger(new Logger.ErrorLogger())
                .logLevel(feignLogLevel)
                .target(ICurrencyAPI.class, currencyAPIUrl);
    }
}