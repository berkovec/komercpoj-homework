package com.komercpoj.controller;

import com.komercpoj.model.vo.CurrencyConversion;
import com.komercpoj.model.vo.Form;
import com.komercpoj.rest.ICurrencyAPI;
import com.komercpoj.service.ICostsService;
import com.komercpoj.service.hl.ISalaryHLService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Salary form controller.
 */
@Controller
public class FormController {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FormController.class);

    /**
     * High level salary service.
     */
    private ISalaryHLService salaryHLService;

    /**
     * Costs service.
     */
    private ICostsService costsService;

    /**
     * Currency conversion REST API client.
     */
    private ICurrencyAPI currencyAPI;

    @Autowired
    public FormController(ISalaryHLService salaryHLService, ICostsService costsService, ICurrencyAPI currencyAPI) {
        this.salaryHLService = salaryHLService;
        this.costsService = costsService;
        this.currencyAPI = currencyAPI;
    }

    /**
     * Form view.
     *
     * @return view name
     */
    @RequestMapping("/")
    public String form(Model model) {
        LOGGER.info("Form entered");

        model.addAttribute("form", new Form());
        return "form";
    }

    /**
     * Submit form action.
     *
     * @param form  form object
     * @param model model
     * @return view name
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String submitForm(@ModelAttribute("form") Form form, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            LOGGER.error("There are validation errors in form: " + bindingResult.getAllErrors());
            model.addAttribute("form", form);
        } else {
            LOGGER.info("Form submitted");

            CurrencyConversion currConv = currencyAPI.latest("CZK");
            float costsCZK = costsService.getCosts(form.getBirtLocalDate(), form.getSalary(), 1f);
            float costsEUR = costsService.getCosts(form.getBirtLocalDate(), form.getSalary(), currConv.getRates().getRateEUR());
            float costsUSD = costsService.getCosts(form.getBirtLocalDate(), form.getSalary(), currConv.getRates().getRateUSD());
            form.setCostsCZK(costsCZK);
            form.setCostsEUR(costsEUR);
            form.setCostsUSD(costsUSD);

            salaryHLService.storeSalary(form);

            model.addAttribute("form", form);
        }
        return "form";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}
