package com.komercpoj.rest;

import com.komercpoj.model.vo.CurrencyConversion;
import feign.Param;
import feign.RequestLine;

/**
 * Feign mapping for currency conversion rates REST API.
 * @see <a href="http://fixer.io">http://fixer.io</a>
 * Created by Michal Berkovec on 30.11.2016.
 */
public interface ICurrencyAPI {

    @RequestLine("GET http://api.fixer.io/latest?base={baseCurrency}")
    CurrencyConversion latest(@Param("baseCurrency")String baseCurrency);
}
