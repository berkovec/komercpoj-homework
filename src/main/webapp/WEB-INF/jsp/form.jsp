<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Person annual salary form</title>
</head>
<body>

<h2>Person salary form</h2>
<form:form method="POST" action="/" modelAttribute="form">
    <table>
        <tr>
            <td><form:label path="birthDate">Birth date</form:label></td>
            <td><form:input path="birthDate" class="date"/></td>
        </tr>
        <tr>
            <td><form:label path="salary">Salary</form:label></td>
            <td><form:input path="salary"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"/>
            </td>
        </tr>
        <tr>
            <td><form:label path="costsCZK">Costs CZK</form:label></td>
            <td><form:input path="costsCZK" readonly="true"/></td>
        </tr>
        <tr>
            <td><form:label path="costsEUR">Costs EUR</form:label></td>
            <td><form:input path="costsEUR" readonly="true"/></td>
        </tr>
        <tr>
            <td><form:label path="costsUSD">Costs USD</form:label></td>
            <td><form:input path="costsUSD" readonly="true"/></td>
        </tr>
    </table>
</form:form>
</body>
</html>