# Komercni pojistovna - homework

## What you'll need
- JDK 1.8 or later
- Maven 3 or later

## Stack
- Spring Boot
- Hibernate
- H2
- REST

## Run
`mvn clean install spring-boot:run`

## Form
- Birth date has to be in format MM-dd-YYYY